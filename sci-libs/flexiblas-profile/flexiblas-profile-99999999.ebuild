# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils cmake-utils alternatives-2

DESCRIPTION="A flexible BLAS wrapper with runtime replacable backends and basic profiling capabilities"

HOMEPAGE="http://www.mpi-magdeburg.mpg.de/mpcsc/software/flexiblas/"

if [[ ${PV} == "99999999" ]] ; then
	ESVN_REPO_URI="http://svncsc.mpi-magdeburg.mpg.de/repos/mess/cmess/trunk"
	inherit subversion
	KEYWORDS=""
else
	SRC_URI="http://www.mpi-magdeburg.mpg.de/mpcsc/software/flexiblas/flexiblas-${PV}.tgz"
	KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"
fi

# License of the package.  This must match the name of file(s) in
# /usr/portage/licenses/.  For complex license combination see the developer
# docs on gentoo.org for details.
LICENSE="GPLv3"

SLOT="0"


IUSE="+atlas -mkl +openblas -gotoblas -acml +threads +cblas"

# A space delimited list of portage features to restrict. man 5 ebuild
# for details.  Usually not needed.
#RESTRICT="strip"

DEPEND="
	virtual/blas
	atlas? ( sci-libs/atlas )
	mkl? ( sci-libs/mkl )
	openblas? ( sci-libs/openblas )
	gotoblas? ( sci-libs/gotoblas2 )
	acml? ( sci-libs/acml )"

# Run-time dependencies. Must be defined to whatever this depends on to run.
# The below is valid if the same run-time depends are required to compile.
RDEPEND="${DEPEND}"
src_unpack(){
	unpack ${A}
	mv flexiblas-${PV} flexiblas-profile-${PV}
}

src_configure() {
	PROFILING="-DPROFILE=ON"
	#MKL check
	if use amd64; then
		if use mkl ; then
			MKL="-DINTEL_64LP=`pkg-config --libs mkl64-gfortran-openmp|sed -e 's/[ \t]*\$//' -e 's/ /;/g'` -DINTEL_64LP_SEQ=`pkg-config --libs mkl64-gfortran|sed -e 's/[ \t]*\$//' -e 's/ /;/g'` -DINTEL_32=OFF -DINTEL_32_SEQ=OFF"
		else
			MKL="-DINTEL_64LP=OFF -DINTEL_64LP_SEQ=OFF"
		fi
	fi
	if use x86; then
		if use mkl ; then
			MKL="-DINTEL_32=`pkg-config --libs mkl32-gfortran-openmp|sed -e 's/[ \t]*\$//' -e 's/ /;/g'` -DINTEL_32_SEQ=`pkg-config --libs mkl32-gfortran|sed -e 's/[ \t]*\$//' -e 's/ /;/g'` -DINTEL_64LP=OFF -DINTEL_64LP_SEQ=OFF"
		else
			MKL="-DINTEL_32=OFF -DINTEL_32_SEQ=OFF"
		fi
	fi
	#ATLAS check
	if use atlas ; then
	 	local ATL=normal
	    local X=`pkg-config --libs atlas-blas-threads 2>/dev/null 1>/dev/null && echo ok`
		if  [ "X$X" = "Xok" ]; then 
			if use threads ; then 
				ATL="thread"
			else 
				ATL=normal
			fi
		else 
			ATL=normal
		fi
		echo "ATLAS: $ATL"
		if  [ "$ATL" = "thread" ] ; then
			echo "Use threaded ATLAS" 
			ATLAS="-DATLAS=`pkg-config --libs atlas-blas-threads|sed -e 's/[ \t]*\$//' -e 's/ /;/g'`"
		else
			echo "Use normal ATLAS" 
			ATLAS="-DATLAS=`pkg-config --libs atlas-blas|sed -e 's/[ \t]*\$//' -e 's/ /;/g'`"
		fi
	else
		ATLAS="-DATLAS=OFF"
	fi
	#OpenBLAS check
	if use openblas ; then
		OPENBLAS="-DOPENBLAS=`pkg-config --libs openblas-threads|sed -e 's/[ \t]*\$//' -e 's/ /;/g'`"
	else
		OPENBLAS="-DOPENBLAS=OFF"
	fi
	#GotoBlas check
	if use gotoblas ;  then
		GOTOBLAS="-DGOTO=`pkg-config --libs gotoblas2-threads|sed -e 's/[ \t]*\$//' -e 's/ /;/g'`"
	else
		GOTOBLAS="-DGOTO=OFF"
	fi
	# ACML check
	if use acml ;  then
		ACML="-DACML=`pkg-config --libs acml|sed -e 's/[ \t]*\$//' -e 's/ /;/g'`"
	else
		ACML="-DACML=OFF -DACML_MP=OFF"
	fi

	if use cblas ; then 
		CBLAS="-DCBLAS=ON"
	else
		CBLAS="-DCBLAS=OFF"
	fi
	#Collecting the resulting cmake args
	local mycmakeargs=(
		${PROFILING} ${MKL} ${ATLAS} ${OPENBLAS} ${GOTOBLAS} ${ACML} -DSYSCONFDIR=/etc -DAPPLE=OFF ${CBLAS}
		)

		cmake-utils_src_configure
}

src_compile() {
		cmake-utils_src_compile
}

src_test() {
		cmake-utils_src_test
}

src_install() {
	cmake-utils_src_install
	alternatives_for blas flexiblas-profile 9 \
		/usr/$(get_libdir)/pkgconfig/blas.pc flexiblas-profile.pc
	if use cblas ; then
	     alternatives_for cblas flexiblas-profile 9 \
		/usr/$(get_libdir)/pkgconfig/cblas.pc flexiblas-profile.pc
	fi
}
