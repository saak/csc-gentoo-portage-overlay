# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils cmake-utils alternatives-2

DESCRIPTION="A flexible BLAS wrapper with runtime replacable backends and basic profiling capabilities"

HOMEPAGE="http://www.mpi-magdeburg.mpg.de/mpcsc/software/flexiblas/"

if [[ ${PV} == "99999999" ]] ; then
        ESVN_REPO_URI="http://svncsc.mpi-magdeburg.mpg.de/repos/mess/cmess/trunk"
        inherit subversion
        KEYWORDS=""
else
	#SRC_URI="http://drittelhacker.no-ip.biz/~saak/mess/${P}.tgz"
	SRC_URI="http://www.mpi-magdeburg.mpg.de/mpcsc/software/mess/${P}.tgz"
	KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"
fi

# License of the package.  This must match the name of file(s) in
# /usr/portage/licenses/.  For complex license combination see the developer
# docs on gentoo.org for details.
LICENSE="GPLv3"

SLOT="0"

IUSE="test doc"

# A space delimited list of portage features to restrict. man 5 ebuild
# for details.  Usually not needed.
#RESTRICT="strip"

DEPEND="
	doc? ( >=app-doc/doxygen-1.8 )
	virtual/pkgconfig
	virtual/blas
	virtual/lapack
	sci-libs/umfpack
	sci-libs/suitesparse
	sci-libs/suitesparseconfig
	sci-libs/amd
	sci-libs/colamd
	sci-libs/camd
	sci-libs/ccolamd
	sci-libs/cholmod
	sci-libs/parmetis
	app-arch/bzip2
	sys-libs/zlib
	sys-libs/glibc
	x11-libs/libX11
	x11-libs/libXpm
	x11-libs/libxcb
	x11-libs/libXau
	x11-libs/libXdmcp"

DEPEND="${RDEPEND}
	>=dev-util/cmake-2.6"


src_configure() {
	local mycmakeargs=(
            -DSUITESPARSE=/usr
        )
        cmake-utils_src_configure
} 

src_compile() {
        cmake-utils_src_compile
}

src_test() {
        cmake-utils_src_test
}

src_install() {
	cmake-utils_src_install
}
