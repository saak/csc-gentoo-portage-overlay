# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils cmake-utils alternatives-2

DESCRIPTION="A flexible BLAS wrapper with runtime replacable backends and basic profiling capabilities"

HOMEPAGE="http://www.mpi-magdeburg.mpg.de/mpcsc/software/flexiblas/"

SRC_URI="http://www.mpi-magdeburg.mpg.de/mpcsc/software/flexiblas/${P}.tar.gz"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"

# License of the package.  This must match the name of file(s) in
# /usr/portage/licenses/.  For complex license combination see the developer
# docs on gentoo.org for details.
LICENSE="GPLv3"

SLOT="0"


IUSE="+atlas -mkl +openblas -gotoblas -acml +threads +cblas +lapack +deprecated -int64"

# A space delimited list of portage features to restrict. man 5 ebuild
# for details.  Usually not needed.
#RESTRICT="strip"
RESTRICT="primaryuri" #Try the primary URI before falling back to mirrors

# Run-time dependencies. Must be defined to whatever this depends on to run.
RDEPEND="
	openblas? ( >=sci-libs/openblas-0.2.11[-dynamic,threads?,int64?] )
	gotoblas? ( sci-libs/gotoblas2[threads?,int64?] )
	mkl? ( sci-libs/mkl[int64?] )
	!int64? (
	   acml? ( sci-libs/acml )
	   atlas? ( sci-libs/atlas[fortran,deprecated?,lapack,threads?] )
	)
	sys-devel/gcc[fortran,nptl]
	sys-libs/glibc"


# Build-time dependencies. Takes the above and adds additional build time dependencies.
DEPEND="
	${RDEPEND}
	dev-util/cmake"

# Prevent the global flexiblasrc from getting overwritten
CONFIG_PROTECT="/etc"

src_configure() {
	#MKL check
	if use amd64; then
		if use mkl ; then
			source $(find /opt/intel/ -name compilervars_arch.sh) intel64
			source $(find /opt/intel/ -name mklvars.sh) intel64
			MKL="-DINTEL_64LP=`pkg-config --libs mkl64-gfortran-openmp|sed -e 's/[ \t]*\$//' -e 's/ /;/g'` -DINTEL_64LP_SEQ=`pkg-config --libs mkl64-gfortran|sed -e 's/[ \t]*\$//' -e 's/ /;/g'` -DINTEL_32=OFF -DINTEL_32_SEQ=OFF -DMKL_CUSTOM=ON"
		else
			MKL="-DINTEL_64LP=OFF -DINTEL_64LP_SEQ=OFF -DMKL_CUSTOM=OFF"
		fi
	fi
	if use x86; then
		if use mkl ; then
			source $(find /opt/intel/ -name compilervars_arch.sh) ia32
			source $(find /opt/intel/ -name mklvars.sh) ia32
			MKL="-DINTEL_32=`pkg-config --libs mkl32-gfortran-openmp|sed -e 's/[ \t]*\$//' -e 's/ /;/g'` -DINTEL_32_SEQ=`pkg-config --libs mkl32-gfortran|sed -e 's/[ \t]*\$//' -e 's/ /;/g'` -DINTEL_64LP=OFF -DINTEL_64LP_SEQ=OFF -DMKL_CUSTOM=ON"
		else
			MKL="-DINTEL_32=OFF -DINTEL_32_SEQ=OFF -DMKL_CUSTOM=OFF"
		fi
	fi
	#ATLAS check
	if use atlas ; then
		local ATL=normal
		local X=`pkg-config --libs atlas-blas-threads 2>/dev/null 1>/dev/null && echo ok`
		if  [ "X$X" = "Xok" ]; then
			if use threads ; then
				ATL="thread"
			else
				ATL=normal
			fi
		else
			ATL=normal
		fi
		echo "ATLAS: $ATL"
		if  [ "$ATL" = "thread" ] ; then
			echo "Use threaded ATLAS"
			ATLAS="-DATLAS=`pkg-config --libs atlas-blas-threads|sed -e 's/[ \t]*\$//' -e 's/ /;/g'`"
		else
			echo "Use normal ATLAS"
			ATLAS="-DATLAS=`pkg-config --libs atlas-blas|sed -e 's/[ \t]*\$//' -e 's/ /;/g'`"
		fi
	else
		ATLAS="-DATLAS=OFF"
	fi
	#OpenBLAS check
	if use openblas ; then
		OPENBLAS="-DOPENBLAS=`pkg-config --libs openblas-threads|sed -e 's/[ \t]*\$//' -e 's/ /;/g'`"
	else
		OPENBLAS="-DOPENBLAS=OFF"
	fi
	#GotoBlas check
	if use gotoblas ;  then
		GOTOBLAS="-DGOTO=`pkg-config --libs gotoblas2-threads|sed -e 's/[ \t]*\$//' -e 's/ /;/g'`"
	else
		GOTOBLAS="-DGOTO=OFF"
	fi
	# ACML check
	if use acml ;  then
		ACML="-DACML=`pkg-config --libs acml|sed -e 's/[ \t]*\$//' -e 's/ /;/g'`"
	else
		ACML="-DACML=OFF -DACML_MP=OFF"
	fi

	if use cblas ; then
		CBLAS="-DCBLAS=ON"
	else
		CBLAS="-DCBLAS=OFF"
	fi
	if use lapack ; then
		LAPACK="-DLAPACK=ON"
		if use deprecated ; then
		   LAPACK_API_VERSION="-DLAPACK_API_VERSION=3.7.0"
		else
		   LAPACK_API_VERSION="-DLAPACK_API_VERSION=3.7.0-wodprc"
		fi
	else
		LAPACK="-DLAPACK=OFF"
		LAPACK_API_VERSION STREQUAL=""
	fi
	#Collecting the resulting cmake args
	local mycmakeargs=(
		${MKL} ${ATLAS} ${OPENBLAS} ${GOTOBLAS} ${ACML} -DSYSCONFDIR=/etc -DAPPLE=OFF ${CBLAS} ${LAPACK} ${LAPACK_API_VERSION}
		)

		cmake-utils_src_configure
}

src_compile() {
		cmake-utils_src_compile
}

src_test() {
		cmake-utils_src_test
}

src_install() {
	cmake-utils_src_install
	alternatives_for blas flexiblas 9 \
		/usr/$(get_libdir)/pkgconfig/blas.pc flexiblas.pc
	if use cblas ; then
		 alternatives_for cblas flexiblas 9 \
		/usr/$(get_libdir)/pkgconfig/cblas.pc flexiblas.pc
	fi
	if use lapack ; then
		 alternatives_for lapack flexiblas 9 \
		/usr/$(get_libdir)/pkgconfig/lapack.pc flexiblas.pc
	fi
}
